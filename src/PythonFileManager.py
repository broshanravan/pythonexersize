
class PythonFileManager:

    def mixed_excersize(self):
        fruit_file = open("../text_files/fruits.txt")

        print("contents brfore seek")
        content = fruit_file.read()
        print(content)
        fruit_file.seek(0)
        print("contents after seek")
        content = fruit_file.read()
        print(content)
        fruit_file.close();

        pin_file = open("../text_files/pins.txt")
        content = pin_file.read()
        print(content)
        pinsMap = content.split(",")
        # data = json.load(pinsMap)
        print(pinsMap)

        numbers = {3, 43, 34, 23, 76, 54, 43}

        for item in numbers:
            if item > 40:
                print(item)

    def write_to_file(self,file_name, myList):
        new_file = open(file_name, "w")
        for item in myList:
            #print(item)
            new_file.write(item)
        new_file.close()


    def amend_file(self, file_name, my_list =[]):
        with open(file_name, "a") as favorit_items_file:
            for item in my_list:
                favorit_items_file.write(item)
        favorit_items_file.close()


    # reads the contents of a text file and counts the number of
    # recurrence of a text within that file
    def find_the_number_of_accourance(self, my_char, fie_name_path = 'my_file'):
        input_file = open(fie_name_path, 'r')
        file_contents = input_file.read()
        count = file_contents.count(my_char)
        return count;

    def read_file_contents(self, fie_name_path = 'my_file'):
        input_file = open(fie_name_path)
        file_contents = input_file.read()
        return file_contents;


    #adding contents of one file to another file
    def add_contents_of_file_to_another(self, file_one_path, file_two_path ):
        with open(file_one_path, 'r') as file_1_content:
            contents = file_1_content.read()

        with open(file_two_path, 'a') as file_two_content:
            file_two_content.write(contents)


pythonFileManager = PythonFileManager()


#translate("rain")

text_to_write = "Bruce\nAreya\nLaleh\nAtoosa"

pythonFileManager.mixed_excersize()

#fileName =input("Please Enter fieName: ")
#python_File_Manager.write_to_file(python_File_Manager,fileName, text_to_write)
##python_File_Manager.mixed_excersize(python_File_Manager)
##
book_list = {"%My  favorit books are:","%Moby Dick","%Art of a deal","%Answer to History","%The Great war of Civilisations","%Civilizations"}
movie_list = {"%My  favorit movies are:", "%Shawshank Redemption", "%Hitman", "%Matrix", "%Despicable me", "%Maze runner"}

#As this is writen in a class we nned to call
#the function from the class or an object of it

pythonFileManager.write_to_file("./text_files/my_file",book_list)

pythonFileManager.amend_file(movie_list,"./text_files/favorits.txt")



count = pythonFileManager.find_the_number_of_accourance("b")
print ("number f recurrance ff the word \'b\' is: " + str(count))

file_content = pythonFileManager.read_file_contents("../favorits.txt")

print(file_content)