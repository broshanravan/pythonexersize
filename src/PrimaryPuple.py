from Student import Student

class PrimaryPuple(Student):
    pass

    def __init__(self ,name, age, sex, high, parentName):
       super().__init__(name, age, sex, high)
       self.parentName = parentName

    def display(self):
        print('Parent Name: ', self.parentName)
        print('School name: ', self.school)
