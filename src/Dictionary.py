import json
from difflib import get_close_matches

class Dictionary:

    def translate(self, w):
        data = json.load(open("../JsonFiles/dictionary.json"))
        w = w.lower()

        matches = get_close_matches(w, data.keys())
        print(matches)

        if w in data:
            return data[w]
        elif w.title() in data:
            return data[w.title()]
        elif w.upper() in data: #in case user enters words like USA or NATO
            return data[w.upper()]
        elif len(get_close_matches(w, data.keys())) > 0:
            yn = input("Did you mean '%s' instead? Enter Y if yes, or N if no: " % get_close_matches(w, data.keys())[0]).upper()
            if yn == "Y":
                return data[get_close_matches(w, data.keys())[0]]
            elif yn == "N":
                if len(get_close_matches(w, data.keys())) > 1:
                    yn_2 = input( "How about '%s' instead? Enter Y if yes, or N if no: " % get_close_matches(w, data.keys())[1]).upper()
                    if yn_2 == "Y":
                        return data[get_close_matches(w, data.keys())[1]]
                    elif yn_2 == "N":
                        return "The word doesn't exist. Please double check it."
            else:
                return "We didn't understand your entry."
        else:
            return "The word doesn't exist. Please double check it."





    def get_meaning(self, word):
        output = self.translate(word)
        if type(output) == list:
            for item in output:
                print(item)
        else:
            print(output)


dictionary = Dictionary()
word = input("Enter word: ")
dictionary.get_meaning(word)
    #get_meaning(word)

