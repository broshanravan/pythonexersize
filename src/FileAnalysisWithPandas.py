import pandas as pd
import numpy as np
import json
import matplotlib.pyplot as plt
from pandas import DataFrame


class FileAnalysisWithPands:

    def __init__(self):
        global app_data
        # reading and importing apple stocks csv from the file at the url
        # https://apmonitor.com/che263/uploads/Main/AAPL.csv

        app_data = pd.read_csv("https://apmonitor.com/che263/uploads/Main/AAPL.csv")

        # This will be done only one within the init function
        # and declared as a global variable for all functions to use

    def plotGraphFromCSVContents(self):

        # setting the size (width,, height) of the graph
        plt.figure(figsize=(10,5))

        # selecting X column (In here volume) against 3 Y column
        # The second attribute determines the color of the graph line
        # The third attribute determines the width of the graph line
        plt.plot(app_data['Volume'],app_data['High'],'k-',linewidth=3)
        plt.plot(app_data['Volume'], app_data['Low'], 'r--', linewidth=3)
        plt.plot(app_data['Volume'], app_data['Close'], 'B', linewidth=3)

        # rotating the values shown for x axis for 90 degrees in order for it to become more readable
        plt.xticks(rotation=90)
        plt.legend(loc='best')
        plt.show()


    def analyzsCSV(self):
        print("start analyzing the csv")

        head = app_data.head(23)
        desc =head.describe()
        print(desc)

        volume_mean = app_data.Volume.mean()
        volume_max = app_data.Volume.max()

        print("\n volumes men is:", volume_mean)
        print("\n volumes max is::", volume_max)

    # This functionaliy will get the value or the  data
    def analyseIndividualCsvVals(self):

        #mean = app_data.mean
        min = np.min(app_data)

        median = np.median

        #print("\nThe mean for volumes is: ", mean)
        print("\nminimum values for each column is:", min)
        print("\nmedian values for each column is:", median)


    def analyseDictionary(self):
        max_age = 0
        min_age = 0

        data = pd.read_json("../JsonFiles/users.json")

        #DataFrame.plot(data['users']['age'],data['users']['ExamMark'])

        for user in data['users']:
            age = user['age'];
            if age > max_age:
                max_age = age

            if age < min_age or min_age == 0:
                min_age = age
            print(user['firstName'], user['age'])

        print("maximum age is: ", max_age)
        print("minimum age is: ", min_age)

        print(data)


        dataFrame = pd.DataFrame(data)
        print(dataFrame)
        print("=================================== Inner data ==================================================")
        innerDataFrame = pd.DataFrame(dataFrame)
        print(innerDataFrame)

        print("The data type is:", type(data), "\n\n")
        print(data.head())
        json_data = data.head() #json.load(open("../JsonFiles/users.json"))
        print(json_data)
        for users in json_data:
            print(users)
            #userdata = users
            #for user_data in userdata:
                #print(user_data)


fileAnalysisWithPandas = FileAnalysisWithPands()

fileAnalysisWithPandas.analyseDictionary()
#fileAnalysisWithPandas.analyseIndividualCsvVals()

#fileAnalysisWithPandas.analyzsCSV()
#fileAnalysisWithPandas.plotGraphFromCSVContents()