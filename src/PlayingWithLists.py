
'''
python Datatypes are:
number, string , lists, tulips and Dictionaties
'''

print('Hello python data types')

grocery_list =['juice','Bread', 'Banana', 'Milk','cerial', 'meat', 'egg']

print('The first item is', grocery_list[0])

print(grocery_list[2:5])

grocery_list.append('Orange')

other_tasks = ['wash car','clean garage','cut lawn']

super_list =[grocery_list,other_tasks]

super_list[0].insert(1, 'onion')

super_list[0].append('Apple')

print('the complete list is:', super_list)

sub_list =  super_list[1:3]
print("sub List is :", end="")
print(sub_list)

mergedList = grocery_list + other_tasks

print("Merged List is : ", mergedList)

mergedList.sort()

print("Sorted List is : ", mergedList)

mergedList.reverse()

print("Reverce sorted List is : ", mergedList)

print('lenght of merged list is:', len(mergedList))
print('Max of merged list is:', max(mergedList))
print('Min of merged list is:', min(mergedList))

#mapps and dictionaries

print('------------------------------Persian Manarcies---------------------------------')

Persian_Monarcies = {'Akhamenian': 'Cyrus The grate', 'Sasanian':
                'Anooshirvan', 'Afsharian' :'NaderShah', 'Safavi' :'Shah Abbas',
                 'ghajaarian': 'Nasereddin Shah'}

print(Persian_Monarcies)

del Persian_Monarcies['ghajaarian']

print(Persian_Monarcies)

Persian_Monarcies.update({'Pahlavi': 'Reza Shah The Grate'})

print(Persian_Monarcies)

print("Akhaminnian King is: ", Persian_Monarcies.get('Akhamenian'))
print('The keys are: ', Persian_Monarcies.keys())

print('values are:')

print(Persian_Monarcies.values())
