import pandas

class PandasTest:


    def sortCollections(self):
        print('\nstart sorting collection')
        def_1 = pandas.DataFrame([[1, 2, 5], [4, 6, 9]], columns=["col_1","col_2","col_3"], index=["row_1","row_2"])
        print(def_1)

    def sortDiscionaries(self):
        print('\nstart sorting Dictionary')
        dict_1 =pandas.DataFrame([
                                      {"First name":"Laleh","Surname":"Yazdy","Job title":"System Security Consultant","Age":41,"Mark":556},
                                      {"First name":"Areya","Surname":"Roshanravan","Job title":"Student","Age":14,"Mark":22},
                                      {"First name":"Atoosa","Surname":"Roshanravan","Job title":"Korre ye Shahr e maa","Age":5,"Mark":45},
                                      {"First name":"Bruce","Surname":"Roshanravan","Job title":"JAVA Developer","Mark":73,"Age":52}
                                ])
        print(dict_1)

        age_mean = dict_1.Age.mean()
        mark_mean = dict_1.Mark.mean()

        print("\nMeans of relevant columns are:")
        print("\nAge mean:", age_mean)
        print("\n Earned points mean:", mark_mean)


    def square(self, num):
        sqr = num * num
        return sqr

    def yieldOnList(self):
        print('\nIn Yield On List')
        numbers = (2, 5, 4, 7, 9)
        for i in numbers:
            print(i)
#        ans = yield i + 2
#        print(ans)



pandasTest = PandasTest()

pandasTest.sortCollections()
pandasTest.sortDiscionaries()
pandasTest.yieldOnList()

#pandasTest.callfunctionOnList()