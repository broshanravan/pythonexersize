
class GeneralTest:

    def callfunctionOnList(self):
        numbers = (2, 5, 4, 7, 9)
        for i in numbers:
            sqr = self.square(i)
            print(sqr)


    def chechInt(self):
        colors = [11, 34.1, 98.2, 43, 45.1, 54, 54]

        for color in colors:
            check_int = isinstance(color, float)
            if (check_int):
                print(color)

    def only_nums_and_zero(self,original_list):

        new_list = [member if not isinstance(member, str) else 0 for member in original_list]
        return new_list

    def get_sum(self, my_list):

        sum = 0.0
        for num_string in my_list:
            sum = sum + float(num_string)

        return sum


    #changing elements of an array to upper
    # case and sorting them alphabetically
    def to_upper_array(self, *args):
        upper_array = []
        for arg in args:
            upper_array.append(arg.upper())
        upper_array.sort()
        return upper_array

    #using keywords for pased in arguments
    ##like a =1, b = 2
    def find_sum(self,**kwargs):
        return sum(kwargs.values())

    def find_multiple(self, a, d, b=4, c =10):
        return a * b * c


generalTest = GeneralTest()

my_original_list = [12,'hello', 24, 54,'how', 'are' , 34,'you']

final_list = generalTest.only_nums_and_zero( my_original_list)

for member in final_list:
    print(member)

sum = generalTest.get_sum(final_list)

print ("Sum of Numbers is: ", sum)

multiple = generalTest.find_multiple(10,2)

print ("multiple is :", multiple)

#

#generalTest.chechInt()

